# Onboarding

This is a small curriculum of onboarding sessions and resources for SREs.

There is a foundational synchronous session to set the stage. The rest of the
documents are intended as self-guided explorations with a synchronous follow-up
discussion to clarify questions.

- [Session: Application architecture](architecture.md)
- [Exploration: Kubernetes at GitLab](gitlab.com_on_k8s.md)
- [Exploration: Diagnosis with Kibana](kibana-diagnosis.md)
- Exploration: Prometheus
- Exploration: Patroni and Postgres
- More areas to cover
  - Sidekiq
  - Gitaly
  - Redis
  - Pages
  - CI
  - Canary
  - Deployer
  - Maintaining Elasticsearch
