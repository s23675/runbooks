# WARNING. DO NOT EDIT THIS FILE BY HAND. USE ./thanos-rules-jsonnet/service-component-alerts.jsonnet TO GENERATE IT
# YOUR CHANGES WILL BE OVERRIDDEN
groups:
- name: 'Service Component Alerts: kube'
  interval: 1m
  partial_response_strategy: warn
  rules:
  - alert: KubeServiceApiserverTrafficCessation
    for: 5m
    annotations:
      title: >-
        The apiserver SLI of the kube service (`{{ $labels.stage }}` stage) has not received any traffic
        in the past 30 minutes
      description: |
        The Kubernetes API server validates and configures data for the api objects which include pods, services, and others. The API Server services REST operations and provides the frontend to the cluster's shared state through which all other components interact.

        This SLI measures all non-health-check endpoints. Long-polling endpoints are excluded from apdex scores.

        This alert signifies that the SLI is reporting a cessation of traffic, but the signal is not absent.
      grafana_dashboard_id: kube-main/kube-overview
      grafana_dashboard_link: >-
        https://dashboards.gitlab.net/d/kube-main/kube-overview?from=now-6h/m&to=now-1m/m&var-environment={{
        $labels.environment }}&var-stage={{ $labels.stage }}
      grafana_min_zoom_hours: '6'
      grafana_panel_id: '3184144040'
      grafana_variables: environment,stage
      promql_template_1: |
        sum by (env,environment,tier,stage) (
          rate(apiserver_request_total{environment="{{ $labels.environment }}",job="apiserver",scope!="",stage="{{ $labels.stage }}"}[5m])
        )
      runbook: docs/kube/README.md
    labels:
      aggregation: component
      alert_class: traffic_cessation
      alert_type: cause
      feature_category: not_owned
      pager: pagerduty
      product_stage: not_owned
      product_stage_group: not_owned
      rules_domain: general
      severity: s2
      sli_type: ops
      slo_alert: 'no'
      user_impacting: 'no'
    expr: >
      gitlab_component_ops:rate_30m{type="kube", component="apiserver", stage="main", monitor="global"}
      == 0
  - alert: KubeServiceApiserverTrafficAbsent
    for: 30m
    annotations:
      title: >-
        The apiserver SLI of the kube service (`{{ $labels.stage }}` stage) has not reported any traffic
        in the past 30 minutes
      description: |
        The Kubernetes API server validates and configures data for the api objects which include pods, services, and others. The API Server services REST operations and provides the frontend to the cluster's shared state through which all other components interact.

        This SLI measures all non-health-check endpoints. Long-polling endpoints are excluded from apdex scores.

        This alert signifies that the SLI was previously reporting traffic, but is no longer - the signal is absent.

        This could be caused by a change to the metrics used in the SLI, or by the service not receiving traffic.
      grafana_dashboard_id: kube-main/kube-overview
      grafana_dashboard_link: >-
        https://dashboards.gitlab.net/d/kube-main/kube-overview?from=now-6h/m&to=now-1m/m&var-environment={{
        $labels.environment }}&var-stage={{ $labels.stage }}
      grafana_min_zoom_hours: '6'
      grafana_panel_id: '3184144040'
      grafana_variables: environment,stage
      promql_template_1: |
        sum by (env,environment,tier,stage) (
          rate(apiserver_request_total{environment="{{ $labels.environment }}",job="apiserver",scope!="",stage="{{ $labels.stage }}"}[5m])
        )
      runbook: docs/kube/README.md
    labels:
      aggregation: component
      alert_class: traffic_cessation
      alert_type: cause
      feature_category: not_owned
      pager: pagerduty
      product_stage: not_owned
      product_stage_group: not_owned
      rules_domain: general
      severity: s2
      sli_type: ops
      slo_alert: 'no'
      user_impacting: 'no'
    expr: |
      gitlab_component_ops:rate_5m{type="kube", component="apiserver", stage="main", monitor="global"} offset 1h
      unless
      gitlab_component_ops:rate_5m{type="kube", component="apiserver", stage="main", monitor="global"}
